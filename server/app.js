/**
 * Module dependencies.
 */

var express = require('express')
    , http = require('http')
    , path = require('path')
    , fs = require('fs')
    , routes = require('./routes/routes')
var app = express();

app.configure(function () {
    app.set('port', process.env.PORT_WebApp || 10086);//cfg.port);
    app.use(express.favicon());
    app.use(express.compress());
    //app.set('view engine', 'html');
    app.engine('html', require('ejs').renderFile);
    app.enable("trust proxy");
    app.use(express.favicon(path.join(__dirname, '../client/images/favicon.ico')));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    app.use(express.session());
    app.use(app.router);
    app.use(express.static(path.join(__dirname, '../client')));
    app.set('views', path.normalize(__dirname + "/../client/views"));
    app.set('view engine', 'html');
});

// 只用于开发环境//app.get('env') = process.env.NODE_ENV
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

// 只用于生产环境
if ('production' == app.get('env')) {

}

routes(app);

http.createServer(app).listen(app.get('port'), function () {
    var open = require('open');
    //open('http://localhost:12306/');
    console.log("Express server listening on port " + app.get('port'));
});
