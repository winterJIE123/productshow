/**
 * Created by Administrator on 14-2-24.
 */
var http = require("http");
var path = require('path');
var baseHttp = require('../common/baseHttp');
var config = require('../config/config');

module.exports = function (app) {
    app.get('/', function(req, res) {
        var html = path.normalize(__dirname + '/../../client/html/index.html');
        res.sendfile(html);
    });
    app.get('/products', function(req, res) {
        var html = path.normalize(__dirname + '/../../client/html/products.html');
        res.sendfile(html);
    });
    app.get('/application',function(req, res){
        var html = path.normalize(__dirname + '/../../client/html/application.html');
        res.sendfile(html);
    });
    
    app.get('/users',function(req, res){
        var html = path.normalize(__dirname + '/../../client/html/users.html');
        res.sendfile(html);
    });
    app.get('/about',function(req, res){
        var html = path.normalize(__dirname + '/../../client/html/about.html');
        res.sendfile(html);
    });

    app.post('/api/apply/applyFor',function(req, res){
        var data = req.body.postValue;
        data = JSON.parse(data);
        var updata = {
            "cmd": "Balm_Askfor_Trial",
            "companyFullName": data.companyName,
            "companyType": data.type,
            "location": {
              "province": data.province,
              "city": data.city
            },
            "proposerName": data.name,
            "job": data.job,
            "sex": data.sex,
            "mobile": data.telephone,
            "tele": data.phone,
            "email": data.email,
            "qqNo": data.qq,
            "wechatNo": data.weixin,
            "remark": data.note
        }
        console.log(updata);
        console.log(config.baseDataApi);
        baseHttp(config.baseDataApi, updata, req, res);
    });

    
};
