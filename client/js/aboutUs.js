/**
 * Created by winter on 14-5-1.
 */
$(function(){
    var titleTar = $(".au_con_title");
    var titleList = titleTar.find("li");
    var iconList = titleTar.find(".icon_arrow");
    var contentList = $(".au_con_con");

    titleList.on("click", function(){

        var index = $(this).index();
        contentList.addClass("hide");
        titleList.removeClass("active");
        iconList.addClass("hide");
        iconList.eq(index).removeClass("hide");
        contentList.eq(index).removeClass("hide");
        titleList.eq(index).addClass("active");
    });

//    var mapsz = new BMap.Map("sz_map");
//    mapsz.centerAndZoom("深圳");
//    mapsz.enableScrollWheelZoom();
//    mapsz.addControl(new BMap.NavigationControl());
//    mapsz.addControl(new BMap.ScaleControl());
//    mapsz.addControl(new BMap.OverviewMapControl());
//    mapsz.addControl(new BMap.MapTypeControl());
//
//    var mapcd = new BMap.Map("cd_map");
//    mapcd.centerAndZoom("成都");
//    mapcd.enableScrollWheelZoom();
//    mapcd.addControl(new BMap.NavigationControl());
//    mapcd.addControl(new BMap.ScaleControl());
//    mapcd.addControl(new BMap.OverviewMapControl());
//    mapcd.addControl(new BMap.MapTypeControl());

    function creatMap( id, city){
        var map = new BMap.Map(id);
        if(city == '成都') {
            var point = new BMap.Point(103.92924898749, 30.762201569306);
        } else {
            var point = new BMap.Point(113.9356777841, 22.51465559054);


        }
       
        map.centerAndZoom(point, 15);
        map.setCenter(point); 
        map.panBy(120,120);
        addMarker(point, map);
        map.addControl(new BMap.NavigationControl());
        map.addControl(new BMap.ScaleControl());
        map.addControl(new BMap.OverviewMapControl());
        map.addControl(new BMap.MapTypeControl());

        map.centerAndZoom(point);
        map.enableScrollWheelZoom();
    }
    function addMarker(point, map) {
        var myIcon = new BMap.Icon("../images/marker.png", new BMap.Size(30, 30), {
            offset: new BMap.Size(21, 21),
            imageOffset: new BMap.Size(0, 0)
        });
        var marker = new BMap.Marker(point, {
            icon: myIcon
        });
        map.addOverlay(marker);
    }
    $(".look-big-map").on("click", function(e){
        var tar = $(e.target || e.srcElement);
        var self = $(this);

        var tagName = tar[0].nodeName;
        if( tagName == "I" || tagName == "SPAN" ){
            var tempCity = $(".pop-map").attr("city");
            var dataCity = self.attr("data-city");

            if(tempCity != dataCity){
                creatMap("pop_map", dataCity);

                var stop = $(document).scrollTop();

                $(".pop-map").css({
                    top: (stop + 50) + "px"
                });
                $(".mask").removeClass("hide");
                $(".pop-map").removeClass("hide");

            }
        }
    });

    $(".icon_delete").on("click", function(){
        $(".mask").addClass("hide");
        $(".pop-map").addClass("hide");
    });

    creatMap("cd_map", "成都");
    creatMap("sz_map", "深圳");

});