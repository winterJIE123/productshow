/**
 * Created by winter on 14-5-3.
 */
$(function () {

    $("#company_name").on("blur", function (e) {
        var name = $(this).val();
        if (name.length == 0) {
            $(this).prev().find(".feedbook").removeClass("hide");
            $(this).focus();
        } else {
            $(this).prev().find(".feedbook").addClass("hide");
        }
    });

    $("#type_other_content").on("blur", function () {
        var value = $("#type_other")[0].checked;

        if (value == true) {
            var str = $(this).val();
            if (str.length == 0) {
                $(this).prev().removeClass("hide");
            } else {
                $(this).prev().addClass("hide");

            }
        }
    });

    $("#company_username").on("blur", function () {
        var str = $(this).val();
        if (str.length == 0) {
            $(this).prev().find(".feedbook").removeClass("hide");
        } else {
            $(this).prev().find(".feedbook").addClass("hide");
        }
    });

    $("#company_job").on("blur", function () {
        var str = $(this).val();
        if (str.length == 0) {
            $(this).prev().find(".feedbook").removeClass("hide");
        } else {
            $(this).prev().find(".feedbook").addClass("hide");
        }
    });

    $("#phone").on("blur", function () {
        var str = $(this).val();
        if (str.length != 0 && (/^1[3|4|5|8][0-9]\d{4,8}$/.test(str))) {
            $(this).prev().find(".feedbook").addClass("hide");

        } else {
            $(this).prev().find(".feedbook").removeClass("hide");
        }
    });

    $("#zuoji_telephone").on("blur", function () {
        var str = $(this).val();
        if (str.length != 0 && /^[+]{0,1}(\d){1,4}[ ]{0,1}([-]{0,1}((\d)|[ ]){1,12})+$/.test(str)) {
            $(this).prev().find("feedbook").addClass("hide");
        } else if (str.length == 0) {
            $(this).prev().find("feedbook").addClass("hide");
        } else {
            $(this).prev().find("feedbook").removeClass("hide");
        }
    });

    $("#Email").on("blur", function () {
        var str = $(this).val();
        if (str.length != 0 && ( /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(str))) {
            $(this).prev().find(".feedbook").addClass("hide");

        } else {
            $(this).prev().find(".feedbook").removeClass("hide");
        }
    });

    $("#qq").on("blur", function () {
        var str = $(this).val();
        if (str.length != 0 && /^[0-9]*[1-9][0-9]*$/.test(str)) {
            $(this).prev().find(".feedbook").addClass("hide");
        } else if (str.length == 0) {
            $(this).prev().find(".feedbook").addClass("hide");
        } else {
            $(this).prev().find(".feedbook").removeClass("hide");
        }
    });

    $("#submit_form").on("click", function () {

        var value = {};
        value.companyName = $("#company_name").val();

        if ($("#type_4s")[0].checked == true) {
            value.type = "4s店";
        } else if ($("#type_qixiu")[0].checked == true) {

            value.type = "汽修店";
        } else {
            value.type = $("#type_other_content").val();
        }

        value.province = $("#select_province").val();
        value.city = $("#select_city").val();
        value.name = $("#company_username").val();
        value.job = $("#company_job").val();

        if ($("#male")[0].checked == true) {
            value.sex = "男";
        } else {
            value.sex = "女";
        }

        value.telephone = $("#phone").val();
        value.phone = $("#zuoji_telephone").val();
        value.email = $("#Email").val();
        value.qq = $("#qq").val();
        value.weixin = $("#wechat").val();
        value.note = $("#remark_note").val();

        var tag = 0;
        if (value.companyName.length == 0) {
            $("#company_name").prev().find(".feedbook").removeClass("hide");
        } else {
            $("#company_name").prev().find(".feedbook").addClass("hide");
            tag++;
        }

        if (value.type.length == 0) {
            $("#type_other_content").prev().removeClass("hide");
        } else {
            $("#type_other_content").prev().addClass("hide");
            tag++;
        }

        if (value.name.length == 0) {
            $("#company_username").prev().find(".feedbook").removeClass("hide");
        } else {
            $("#company_username").prev().find(".feedbook").addClass("hide");
            tag++;
        }

        if (value.job.length == 0) {
            $("#company_job").prev().find(".feedbook").removeClass("hide");
        } else {
            $("#company_job").prev().find(".feedbook").addClass("hide");
            tag++;
        }

        if (value.telephone.length != 0 && (/^1[3|4|5|8][0-9]\d{4,8}$/.test(value.telephone))) {
            $("#phone").prev().find(".feedbook").addClass("hide");
            tag++;
        } else {
            $("#phone").prev().find(".feedbook").removeClass("hide");
        }
        
        if (value.phone.length != 0 && /^[+]{0,1}(\d){1,4}[ ]{0,1}([-]{0,1}((\d)|[ ]){1,12})+$/.test(value.phone.length)) {
            $("#zuoji_telephone").prev().find("feedbook").addClass("hide");
            tag++;
        } else {
            $("#zuoji_telephone").prev().find("feedbook").removeClass("hide");
        }

        if (value.email.length != 0 && /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(value.email)) {
            $("#Email").prev().find(".feedbook").addClass("hide");
            tag++;
        } else {
            $("#Email").prev().find(".feedbook").removeClass("hide");
        }

        if (value.qq != 0 && /^[0-9]*[1-9][0-9]*$/.test(value.qq)) {
            $("#qq").prev().find(".feedbook").addClass("hide");
            tag++;
        } else {
            $("#qq").prev().find(".feedbook").removeClass("hide");
        }
        if (tag == 8) {
            console.log(value);
            $("#submit_form").text('正在提交..');
            $('#submit_form').attr({"disabled":"disabled"});

            $.ajax({
                    url: '/api/apply/applyFor',
                    type: 'post',
                    data: {
                        'postValue': JSON.stringify(value)
                    },
                    success: function(json) {
                        console.log(json);
                        $("#submit_form").text('提交');
                        $("#submit_form").removeAttr("disabled");
                        if(json.errorCode == 0) {
                            $('.feedbook').remove();
                            $('#submit_form').after('<p class="get_ok feedBack">恭喜你，申请成功！我们的客服将会在最短的时间内联系您.</p>');
                        } else {
                            $('.feedbook').remove();
                            $('#submit_form').after('<p class="feedbook feedBack">不好意思，网络除了点问题，请稍后再试...</p>');
                            // _this.$el.find('.validationError').remove()
                            // _this.$el.find('.member_add_item_repsw').after('<span class="validationError">创建失败！</span>');
                        }
                    },  
                    error: function(json) {
                        $("#submit_form").text('提交');
                        $("#submit_form").removeAttr("disabled");
                        console.log(json);
                        // _this.$el.find('.validationError').remove()
                        // _this.$el.find('.member_add_item_QQ').after('<span class="validationError">出现错误！</span>');
                    }
                });
        } else {
            $('#remark_note').after('<span class="feedbook>您的信息不完整</span>');
        }
    });

    $("#city").citySelect();
});